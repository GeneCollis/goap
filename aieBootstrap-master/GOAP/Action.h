#ifndef Action_H
#define Action_H

#include "WorldState.h"

#include <string>

namespace GOAP
{

	class Action
	{
	public:
		Action() = delete;
		Action(std::string a_name) : name(a_name), cost(1.0f), preconditions(WorldState()), effects(WorldState()) {};
		~Action() = default;

		// Returns true if the worldState has all the requirements
		// for the preconditions of this action
		// and doesn't for inverse preconditions
		bool CheckPreconditions(WorldState& worldState)
		{
			// Return if the preconditions are met but not if any of the inverse preconditions are		
			return worldState.Compare(preconditions) && 
				(inversePreconditions.Count() == 0 || !worldState.Compare(inversePreconditions));
		}

		// Applies the effects of this Action to the worldState
		// (first checks if the precondtitions are met, returns false if not)
		bool ApplyEffects(WorldState& worldState)
		{
			if (CheckPreconditions(worldState))
			{
				worldState.Apply(effects);
				return true;
			}
			// Failed
			return false;
		}

		// Adds a precondition with the type T, name and value
		template<typename T>
		void AddPrecondition(const std::string name, const T value)
		{
			preconditions.Add(name, value);
		}

		// Adds a inverse precondition with the type T, name and value
		template<typename T>
		void AddInversePrecondition(const std::string name, const T value)
		{
			inversePreconditions.Add(name, value);
		}

		// Adds a effect with the type T, name and value
		template<typename T>
		void AddEffect(const std::string name, const T value)
		{
			effects.Add(name, value);
		}

		//#pragma region Setters and Getters
		//
		//	void SetPreconditions(WorldState newPreconditions) { preconditions = newPreconditions; }
		//	const WorldState& GetPreconditions() const { return preconditions; }
		//
		//	void SetEffects(WorldState newEffects) { effects = newEffects; }
		//	const WorldState& GetEffects() const { return effects; }
		//
		//#pragma endregion

		// Returns the name of this action
		std::string GetName() const { return name; }

		// Returns the cost of this action
		float GetCost() const { return cost; }

	private:
		// The name of this action
		std::string name;
		// The cost of this action
		float cost;
		// The required world states of this action ( These must be true for the action to work )
		WorldState preconditions;
		// The required world states of this action ( These must be false for the action to work )
		WorldState inversePreconditions;
		// The effects of this action on the world state
		WorldState effects;

	};

}
#endif // !Action_H
