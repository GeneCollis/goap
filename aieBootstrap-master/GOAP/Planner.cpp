#include "Planner.h"

namespace GOAP
{
	bool Planner::CalculatePlan(const WorldState& startState,
		const WorldState& goalState, const std::vector<Action>& actions,
		std::vector<Action>& plan) const
	{
		// Create a empty plan
		plan = std::vector<Action>();

		// Check if the goal is already met
		if (startState.Compare(goalState))
			return true;

		// Generates a graph with the startState, goalState and actions we give 
		Graph graph = GenerateGraph(startState, goalState, actions);

		// If the graph that was generated only has 1 or less nodes than there is no plan
		if (graph.nodes.size() < 2)
			return false;

		Node* goalNode = graph.Contains(goalState);
		// Check if the graph doesnt contain the goal state
		if (goalNode == nullptr)
			return false;

		// Search the graph for the best path to the goal
		plan = graph.Dijkstra(goalNode);

		return true;
	}

	Graph Planner::GenerateGraph(const WorldState & startState, const WorldState & goalState, const std::vector<Action>& actions) const

	{
		// If we found the goal state after making the graph
		bool goalStateFound = false;

		// Create a queue of Nodes to traverse
		std::queue<Node*> queue;

		// Create the graph
		Graph graph = Graph();
		graph.startNode = graph.AddNode(startState);

		// Add the start node to the queue
		queue.push(graph.startNode);

		// While there is still nodes to process in the queue
		while (!queue.empty())
		{
			// Get the current Node to process
			Node* currentNode = queue.front();
			queue.pop();

			// For each action
			for (Action action : actions)
			{
				// Check if this action can be performed on the current state
				if (action.CheckPreconditions(currentNode->data))
				{
					// Create a world state that we can check and apply the action to
					WorldState newState(currentNode->data);

					// Apply the effects to the new state
					action.ApplyEffects(newState);

					// Check if we already have this node in our graph
					Node* existingNode = graph.ContainsExact(newState);
					// If the node already exists
					if (existingNode != nullptr)
					{
						// Add the Edge between the nodes
						currentNode->AddEdge(existingNode, action);
					}
					else
					{
						// Add the node to the graph
						Node* newNode = graph.AddNode(std::move(newState));
						// Add the edge
						currentNode->AddEdge(newNode, action);

						// Compare against the goal state
						if (newState.Compare(goalState))
						{
							// TODO we found the goal do something?
							goalStateFound = true;
						}

						// Add the node to the queue to be processed
						queue.push(newNode);
					}
				}
			}
		}


		// Return the graph
		return graph;
	}
}