//#pragma once
#ifndef Planner_H
#define PlannerH

#include "Action.h"
#include "Graph.h"

#include <queue>
#include <vector>
#include <cassert>

//#include <algorithm>

namespace GOAP
{
	class Planner
	{
	public:
		Planner() {};
		~Planner() {};

		// Returns if the a plan was able to be made or not 
		bool CalculatePlan(const WorldState& startState,
			const WorldState& goalState, const std::vector<Action>& actions,
			std::vector<Action>& plan) const;

	private:

		Graph GenerateGraph(const WorldState& startState, const WorldState& goalState,
			const std::vector<Action>& actions) const;



	};
} // Namespace GOAP
#endif // !Planner_H