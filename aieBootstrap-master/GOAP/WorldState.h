#ifndef WorldState_H
#define WorldState_H

#include "WorldValue.h"

// std
#include <map>

namespace GOAP
{

	class WorldState
	{
	public:
		WorldState() : worldState(std::map<std::string, IWorldValue*>()) {};
		~WorldState()
		{
			for (auto value : worldState)
			{
				if (value.second)
					delete value.second;
			}
		};

		// Copy constructor
		WorldState(const WorldState& obj)
		{
			for (auto value : obj.worldState)
			{
				worldState.insert({ value.first, value.second->GetCopy() });
			}
		}
		// Move constructor
		WorldState(WorldState&& obj)
		{
			worldState = obj.worldState;
			obj.worldState.clear();
		}
		// Copy assignment
		WorldState& operator=(const WorldState& obj)
		{
			for (auto value : worldState)
			{
				delete value.second;
			}
			worldState.clear();

			for (auto value : obj.worldState)
			{
				worldState.insert({ value.first, value.second->GetCopy() });
			}

			return *this;
		}
		// Move assignment
		WorldState& operator=(WorldState&& obj)
		{
			for (auto value : worldState)
			{
				delete value.second;
			}

			worldState = obj.worldState;
			obj.worldState.clear();

			return *this;
		}

		// Adds a WorldValue with the type T and name
		template<typename T>
		void Add(const std::string name, const T value)
		{
			worldState.insert({ name, new WorldValue<T>(value) });
		}

		// Sets the value of the WorldValue with name to value
		// If returned false the Set was not performed because
		// the name doesn't exist or Type didn't match the name
		template<typename T>
		bool Set(const std::string name, const T value)
		{
			auto it = worldState.find(name);
			if (it != worldState.end())
			{
				IWorldValue* worldValue = it->second;

				return worldValue->Set<T>(value);
			}

			// Failed to find the WorldValue to set
			return false;
		}

		// Will apply all of the Values that have the same name and type
		bool Apply(const WorldState& other)
		{
			bool hasAnyFailed = false;

			// For each worldValue in the others world state
			for (auto& value : (other.worldState))
			{
				auto it = worldState.find(value.first);
				// If we found a WorldValue with the same name
				if (it != worldState.end())
					// If the type is the same then set the values
					if (worldState[value.first]->Set(value.second))
						continue;

				hasAnyFailed = true;
			}

			return !hasAnyFailed;
		}

		// Returns true if all of the world values in the rhs are 
		// 1. Contained in the lhs
		// 2. Are the same type
		// 3. Are the same value
		// And they have the same amount of world values
		bool ExactCompare(const WorldState& rhs) const
		{
			// Check if they have the same size
			if (worldState.size() != rhs.worldState.size())
				return false;

			for (auto const& value : rhs.worldState)
			{
				auto it = worldState.find(value.first);
				// If we found a WorldValue with this name
				if (it != worldState.end())
					// If the values & type are the same
					if (it->second->Compare(value.second))
						continue;

				return false;
			}

			return true;
		}

		// Returns true if all of the world values in the rhs are 
		// 1. Contained in the lhs
		// 2. Are the same type
		// 3. Are the same value
		bool Compare(const WorldState& rhs) const
		{
			for (auto const& value : rhs.worldState)
			{
				auto it = worldState.find(value.first);
				// If we found a WorldValue with this name
				if (it != worldState.end())
					// If the values & type are the same
					if (it->second->Compare(value.second))
						continue;

				return false;
			}

			return true;
		}

		// Returns how many world values this world state contains
		int Count() const
		{
			return (int)worldState.size();
		}

		// Removes all of the world values
		void Clear()
		{
			worldState.clear();
		}

	private:
		// The list of world values
		std::map<std::string, IWorldValue*> worldState;

	};


}
#endif // !WorldState_H
