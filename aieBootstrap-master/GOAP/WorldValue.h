#ifndef WorldValue_H
#define WorldValue_H

namespace GOAP
{
	// Forward declare WorldValue
	template<typename T>
	class WorldValue;

	class IWorldValue
	{
	public:
		virtual ~IWorldValue() {};

		// Returns a new IWorldValue ptr with the same values as this
		virtual IWorldValue* GetCopy() = 0;

		// Returns if the Get worked or not, If false then the template Type T was incorrect
		template<typename T>
		bool Get(T& a_value)
		{
			// Cast this to a WorldValue with the template that is used in this function
			WorldValue<T>* worldValue = dynamic_cast<WorldValue<T>*>(this);

			// Check if the cast worked
			if (worldValue)
			{
				// Set the value
				a_value = worldValue->Get();
				return true;
			}

			// Failed
			return false;
		}

		// Returns if the Set worked or not, If false then the template Type T was incorrect
		template<typename T>
		bool Set(T a_value)
		{
			// Cast this to a WorldValue with the template that is used in this function
			WorldValue<T>* worldValue = dynamic_cast<WorldValue<T>*>(this);

			// Check if the cast worked
			if (worldValue)
			{
				// Set the value
				worldValue->Set(a_value);
				return true;
			}

			// Failed
			return false;
		}

		// Sets the value of this to the value of other if they are the same type
		virtual bool Set(IWorldValue* other) = 0;

		// Returns true if the other has the same type and value
		virtual bool Compare(IWorldValue* other) = 0;

	protected:
		// This is protected so that you can't create a IWorldValue object
		IWorldValue() {};
	};

	template<typename T>
	class WorldValue : public IWorldValue
	{
	public:
		explicit WorldValue(T a_value) { value = a_value; };

		// Returns a new IWorldValue ptr with the same value as this
		virtual IWorldValue* GetCopy() override
		{
			return new WorldValue<T>(value);
		}

		// Sets the value to newValue
		void Set(T newValue) { value = newValue; }

		// Returns the value
		T Get() { return value; }

		// Sets the value of this to the value of other if they are the same type
		bool Set(IWorldValue* other) override
		{
			T otherValue;
			// Return true if everything succeeds
			if (other->Get<T>(otherValue))
			{
				this->Set(otherValue);
				return true;
			}

			return false;
		}

		// Returns true if the other has the same type and value
		bool Compare(IWorldValue* other) override
		{
			T otherValue;
			// If the other is of the same type
			// AND 
			// if the other has the same value
			return
				other->Get<T>(otherValue)
				&&
				otherValue == value;
		}

	private:
		T value;
	};
}
#endif // !WorldValue_H
