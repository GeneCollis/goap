#ifndef Graph_H
#define Graph_H

#include "Action.h"

#include <algorithm>
#include <vector>

namespace GOAP
{
	// Predeclare Node
	class Node;

	class Edge
	{
	public:
		Edge() = delete;
		Edge(Node* a_start, Node* a_end, Action a_action)
			: start(a_start), end(a_end), action(a_action) {};

		// The connected node at the start
		Node* start;
		// The connected node at the end
		Node* end;
		// The action for this Edge
		Action action;
	};

	class Node
	{
	public:
		Node() = delete;
		Node(WorldState a_data) : data(a_data), edges(std::vector<Edge*>()) {};
		
		Node(const Node& other)
		{
			for (auto& edge : other.edges)
			{
				edges.push_back(new Edge(*edge));
			}
		}

		Node(Node&& other)
		{
			edges = other.edges;
			other.edges.clear();
		}
		
		~Node()
		{
			for (auto& edge : edges)
			{
				delete edge;
			}
		}

		Node& operator=(const Node& other)
		{
			for (auto& edge : edges)
			{
				delete edge;
			}
			edges.clear();
			for (auto& edge : other.edges)
			{
				edges.push_back(new Edge(*edge));
			}
		}

		Node& operator=(Node&& other)
		{
			for (auto& edge : edges)
			{
				delete edge;
			}
			edges = other.edges;
		}

		Edge* AddEdge(Node* a_end, Action action)
		{
			// Create the edge on the heap
			Edge* newEdge = new Edge(this, a_end, action);

			// Add the edge to our edges
			edges.push_back(newEdge);

			// Return the new edge
			return newEdge;
		}

		// The worldState for this Node
		WorldState data;
		// Dynamic array of out edges
		std::vector<Edge*> edges;


		// Has the node been traversed
		bool traversed;
		// In the node is currently is our queue
		bool inQueue;

		// The distance from the startNode
		float gScore;

		// The previous edge in the cheapest current sequence (the edge we came from the get here)
		Edge* prevEdge;
	};

	class Graph
	{
	public:
		Graph() : nodes(std::vector<Node*>()), startNode(nullptr) {};
		Graph(const Graph& other)
		{
			for (auto& node : other.nodes)
			{
				Node* clonedNode = new Node(*node);
				nodes.push_back(clonedNode);
				if (node == other.startNode)
				{
					startNode = clonedNode;
				}
			}
		}
		Graph(Graph&& other)
		{
			nodes = other.nodes;
			startNode = other.startNode;

			other.nodes.clear();
			other.startNode = nullptr;
		}

		~Graph() 
		{
			for (auto& node : nodes)
			{
				delete node;
			}
		};

		Graph& operator=(const Graph& other)
		{
			for (auto& node : nodes)
			{
				delete node;
			}
			nodes.clear();

			for (auto& node : other.nodes)
			{
				Node* clonedNode = new Node(*node);
				nodes.push_back(clonedNode);
				if (node == other.startNode)
				{
					startNode = clonedNode;
				}
			}

			return *this;
		}

		Graph& operator=(Graph&& other)
		{
			for (auto& node : nodes)
			{
				delete node;
			}

			nodes = other.nodes;
			startNode = other.startNode;

			other.nodes.clear();
			other.startNode = nullptr;

			return *this;
		}

		// Returns the node that contains the equivalent world state
		Node* Contains(const WorldState& worldState)
		{
			for (Node* node : nodes)
			{
				if (node->data.Compare(worldState))
				{
					// Found the node
					return node;
				}
			}
			// Didnt find the node
			return nullptr;
		}

		Node* ContainsExact(const WorldState& worldState)
		{
			for (Node* node : nodes)
			{
				if (node->data.ExactCompare(worldState))
				{
					// Found the node
					return node;
				}
			}
			// Didnt find the node
			return nullptr;
		}

		// Adds a graph node to nodes with a_data
		Node* AddNode(const WorldState a_data)
		{
			// Create a new Node on the heap
			// Set the Node data to a_data 
			Node* newNode = new Node(a_data);

			// Add the Node to the nodes
			nodes.push_back(newNode);

			// Return ptr to the new node
			return newNode;
		}

		// Adds a graph node to nodes
		void AddNode(Node* newNode)
		{
			// Add the Node to the nodes
			nodes.push_back(newNode);
		}

		// Adds an edge to a_start and a_end, linking them and vise-versa 
		Edge* AddEdge(Node* a_start, Node* a_end, Action& a_action)
		{
			// Return the new edge
			return a_start->AddEdge(a_end, a_action);
		}


		std::vector<Action> Dijkstra(Node * a_endNode)
		{
			std::vector<Node*> orderedQueue = std::vector<Node*>();

			// Initialze all nodes
			for (auto node : nodes)
			{
				node->traversed = false;
				node->inQueue = false;
				node->gScore = std::numeric_limits<float>::max();
				node->prevEdge = nullptr;
			}

			// Add the start node to process
			orderedQueue.push_back(startNode);
			startNode->gScore = 0;
			startNode->inQueue = true;

			// While we have nodes to process
			while (orderedQueue.size() > 0)
			{
				// Order from biggest to smallest gScore
				std::sort(orderedQueue.begin(), orderedQueue.end(),
					[](const Node* a, const Node* b) -> bool
				{
					return a->gScore > b->gScore;
				});

				// Get the smallest gScore node
				Node* currentNode = orderedQueue.back();
				orderedQueue.pop_back();

				// If we got to the end node
				if (currentNode == a_endNode)
					break; // Stop the while loop we're done

						   // loop through all edges
				for (auto edge : currentNode->edges)
				{
					// Don't bother with nodes that have been traversed
					if (edge->end->traversed)
						continue;
					else
					{
						// Calculate the gScore
						float gScore = currentNode->gScore + edge->action.GetCost();
						// Only change the gScore if it is less
						if (gScore < edge->end->gScore)
						{
							// Set the gScore of the end node to it's new one
							edge->end->gScore = gScore;
							// Set the prevEdge as this is the new cheapest edge
							edge->end->prevEdge = edge;
						}

						// If the node is NOT in the queue add it
						if (!edge->end->inQueue)
						{
							// Add the endNode to the queue
							orderedQueue.push_back(edge->end);
							edge->end->inQueue = true;
						}
					}
				}

				// Set this node as traversed
				currentNode->traversed = true;
			}

			// The plan that we found
			std::vector<Action> plan = std::vector<Action>();

			// Add the actions until we find the start node (this will break if the graph wasn't correct)
			Edge* currentEdge = a_endNode->prevEdge;
			while (currentEdge != nullptr)
			{
				// Add the action for this edge
				plan.push_back(currentEdge->action);
				// Get the new edge
				currentEdge = currentEdge->start->prevEdge;
			}

			// Reverse the plan to be in the correct order
			std::reverse(std::begin(plan), std::end(plan));

			// Return a plan!
			return plan;
		}

		// Dynamic array of the nodes
		std::vector<Node*> nodes;

		Node* startNode;

	};
}
#endif // !Graph_H
