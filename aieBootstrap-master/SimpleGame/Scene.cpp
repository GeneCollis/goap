#include "Scene.h"
#include "GameObject.h"

#include "Input.h"

#include <imgui.h>

#include <algorithm>

//static void ShowHelpMarker(const char* desc)
//{
//	ImGui::TextDisabled("(?)");
//	if (ImGui::IsItemHovered())
//		ImGui::SetTooltip(desc);
//}

Scene::~Scene()
{
	for (auto it = gameObjects->begin(); it != gameObjects->end(); ++it)
	{
		delete (*it);
	}
	gameObjects->clear();
	delete gameObjects;

}

void Scene::Update(const float deltaTime, aie::Input* input)
{
	// Find game objects that should be deleted and delete them
	for (auto& go : *gameObjects)
		if (go->ShouldDestroy())
		{
			delete go;
			go = nullptr;
		}

	// Remove gameObjects if they are nullptr
	gameObjects->erase(std::remove_if(gameObjects->begin(), gameObjects->end(),
		[](const GameObject* go) { return go == nullptr; }), gameObjects->end());

	// Add to the accumulated time
	accumulatedTime += deltaTime * timeScale;

	float scaledTimeStep = timeStep;
	// Scale the time step if we should
	if (scaleTimeStep)
		scaledTimeStep = timeStep * timeScale;

	// Update physics at a fixed time step
	while (accumulatedTime >= scaledTimeStep)
	{
		// FixedUpdate each object
		//for (size_t i = 0; i < gameObjects->size(); i++)
		//	gameObjects->at(i)->FixedUpdate(scaledTimeStep, gravity);

		accumulatedTime -= scaledTimeStep;

		// Check if we are stuck in the loop
		if (timeStep * timeScale == 0)
			break;
	}

	// Update each object
	for (size_t i = 0; i < gameObjects->size(); i++)
		if (gameObjects->at(i)->IsActive())
			gameObjects->at(i)->Update(deltaTime * timeScale, input);
}

void Scene::Draw(aie::Renderer2D * renderer2D)
{
	// Draw each object
	for (size_t i = 0; i < gameObjects->size(); i++)
		if (gameObjects->at(i)->IsActive())
			gameObjects->at(i)->Draw(renderer2D, float(i + 1) * 0.0001f, showDebug);

	// Get the Input instance
	aie::Input* input = aie::Input::getInstance();

}

void Scene::ImGui()
{
	//ImGui::Begin(&name[0]);

	//if (ImGui::CollapsingHeader("Settings"))
	{
		//ImGui::SliderFloat("Time Scale", &timeScale, 0.0f, 2.0f, "%.2f", 1.0f);

		//ImGui::Checkbox("Scale time step", &scaleTimeStep); ImGui::SameLine();
		//ImGui::Checkbox("Show Debug", &showDebug);

		//ImGui::SliderFloat2("Gravity", &gravity[0], -1000.0f, 1000.0f, "%.2f", 1.0f);
	}

	//if (ImGui::CollapsingHeader("GameObject Creation"))
	//{
	//}

	//ImGui::End();

	if (showDebug)
	{
		// Do ImGui for each game object
		for (size_t i = 0; i < gameObjects->size(); i++)
			if (gameObjects->at(i)->IsActive())
				gameObjects->at(i)->ImGui();
	}
}

GameObject * Scene::GetNearbyGameObject(std::string name, glm::vec2 pos)
{
	float closestDist = std::numeric_limits<float>::max();
	GameObject* closestGameObject = nullptr;

	for (auto& go : *gameObjects)
	{
		if (go->GetName() == name)
		{
			float dist = glm::distance(go->GetPosition(), pos) < closestDist;
			if (dist < closestDist)
			{
				closestDist = dist;
				closestGameObject = go;
			}
		}
	}

	return closestGameObject;
}

void Scene::AddGameObject(GameObject * newGameObject)
{
	// If the new gameObject is nullptr don't add it
	assert(newGameObject != nullptr);
	// If the new gameObject is already in this scene don't add it
	assert(std::find(gameObjects->begin(), gameObjects->end(), newGameObject) == gameObjects->end());

	gameObjects->push_back(newGameObject);
	newGameObject->SetScene(this); // Set the scene it is in to this scene
}

void Scene::AddGameObject(GameObject * newGameObject, Transform* parent)
{
	AddGameObject(newGameObject);
}

void Scene::RemoveGameObject(GameObject * gameObjectToRemove)
{
	// If the gameObject is nullptr delete add it
	assert(gameObjectToRemove != nullptr);
	// If the gameObject isn't already in this scene don't delete it
	assert(std::find(gameObjects->begin(), gameObjects->end(), gameObjectToRemove) != gameObjects->end());

	gameObjects->erase(std::find(gameObjects->begin(), gameObjects->end(), gameObjectToRemove));

	delete gameObjectToRemove;
}

