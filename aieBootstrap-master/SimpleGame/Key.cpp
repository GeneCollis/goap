#include "Key.h"
#include "TextureMap.h"

#include <Renderer2D.h>

void Key::Draw(aie::Renderer2D * a_renderer2D, float a_depth, const bool& a_debug) const
{
	if (texture != nullptr)
	{
		a_renderer2D->setRenderColour(1.0f, 1.0f, 1.0f, 1.0f);
		a_renderer2D->setUVRect(uv);
		a_renderer2D->drawSprite(texture, position.x, position.y, TextureMap::TileSize(), TextureMap::TileSize(), 0.0f, a_depth);
	}
	else
	{
		a_renderer2D->setRenderColour(1.0f, 1.0f, 0.0f, 1.0f);

		a_renderer2D->drawCircle(position.x, position.y, radius, a_depth);
		a_renderer2D->drawBox(position.x, position.y + radius * 2, radius, radius * 2.5f, 0, a_depth);
		a_renderer2D->drawBox(position.x + radius, position.y + radius * 3 - 1, radius, radius - 2, 0, a_depth);
		a_renderer2D->drawBox(position.x + radius - 1, position.y + radius * 2, radius - 2, radius - 3, 0, a_depth);
	}
}