#ifndef Scene_H
#define Scene_H

#include <glm\glm.hpp>
#include <vector>

class GameObject;
class Transform;
class Collider;
namespace aie
{
	class Renderer2D;
	class Input;
};

class Scene
{
public:
	Scene(const std::string a_name = "Scene", float a_timeStep = 0.01f,
		const glm::vec2 a_gravity = { 0.0f, 0.0f }) : name(a_name),
		gameObjects(new std::vector<GameObject*>), timeStep(a_timeStep),
		gravity(a_gravity), accumulatedTime(0.0f), timeScale(1.0f),
		scaleTimeStep(false), showDebug(false) {}
	~Scene();

	// Updates all of the gameObjects
	void Update(const float deltaTime, aie::Input* input);
	// Draws all of the gameObjects
	void Draw(aie::Renderer2D* renderer2D);

	// Sets up ImGui for this scene
	void ImGui();

	// Returns a game object near the position with the same name
	GameObject* GetNearbyGameObject(std::string name, glm::vec2 pos);

	// Adds the newGameObject to this scene
	void AddGameObject(GameObject* newGameObject);
	// Adds the newGameObject to this scene and sets its parent
	void AddGameObject(GameObject* newGameObject, Transform* parent);
	// Removes and deletes the gameObject
	void RemoveGameObject(GameObject* gameObjectToRemove);
	// Returns the gameObjects list in this scene
	std::vector<GameObject*>* GetGameObjects() const { return gameObjects; };

	// Sets the gravity for this scene
	void SetGravity(const glm::vec2 a_gravity) { gravity = a_gravity; }
	// Returns the gravity in this scene
	glm::vec2 GetGravity() const { return gravity; }

	// Sets the timeStep 
	void SetTimeStep(const float a_timeStep) { timeStep = a_timeStep; }
	// Returns the timeStep
	float GetTimeStep() const { return timeStep; }

	// Sets showDebug 
	void SetShowDebug(const bool a_showDebug) { showDebug = a_showDebug; }
	// Returns showDebug
	float GetShowDebug() const { return showDebug; }

private:
	// The name of this scene
	std::string name;

	// A list of the GameObjects in this scene
	std::vector<GameObject*>* gameObjects;
	// The gravity in this scene
	glm::vec2 gravity;
	// The physics time step for this scene
	float timeStep;
	// The time to be used on physics time step updates
	float accumulatedTime;
	// The rate at which time flows (1.0f = real time)
	float timeScale;
	// If the time scale affects the time step (this can cause physics to be inconsistent as the time scale will vary)
	bool scaleTimeStep;
	// If the debug things should be shown
	bool showDebug;

};
#endif // !Scene_H
