#ifndef GameObject_H
#define GameObject_H
#include <string>

#include <glm/glm.hpp>

class Scene;
class Tile;
class Grid;

namespace aie
{
	class Input;
	class Renderer2D;
	class Texture;
}

class GameObject
{
public:
	GameObject() = delete;
	GameObject(std::string a_name) : name(a_name), position{ 0, 0 }, radius(1.0f),
		open(true), shouldDestroy(false), isActive(true), texture(nullptr), uv({ 0.0f, 0.0f, 1.0f, 1.0f }) {};
	GameObject(std::string a_name, const glm::vec2 a_position, const float a_radius)
		: name(a_name), position(a_position), radius(a_radius), currentTile(nullptr),
		open(true), shouldDestroy(false), isActive(true), texture(nullptr), uv({0.0f, 0.0f, 1.0f, 1.0f}) {};
	GameObject(std::string a_name, Tile* a_tile, const float a_radius);


	virtual ~GameObject() {};

	virtual void Update(const float deltaTime, aie::Input* input) = 0;
	virtual void Draw(aie::Renderer2D* a_renderer2D, float a_depth = 0.0f, const bool& a_debug = false) const = 0;
	virtual void ImGui() = 0;

	// The Object will be deleted before the next frame 
	void Destroy() { shouldDestroy = true; }

	// If the object is set to be deleted
	bool ShouldDestroy() const { return shouldDestroy; }

	void SetActive(const bool a_active) { isActive = a_active; }
	bool IsActive() const { return isActive; }


#pragma region Getters and Setters

	std::string GetName() const { return name; }

	glm::vec2 GetPosition() const { return position; }
	void SetPosition(const glm::vec2 a_newPos) { position = a_newPos; }

	float GetRadius() const { return radius; }

	Tile* GetCurrentTile() const { return currentTile; }
	void SetCurrentTile(Tile* a_newTile);

	void SetTexture(aie::Texture* a_tex) { texture = a_tex; }
	void SetUV(glm::vec4 a_uv) { uv = a_uv; }

	void SetScene(Scene* a_scene) { scene = a_scene; }

	bool IsOpen() { return open; }

#pragma endregion

protected:
	std::string name;

	glm::vec2 position;
	float radius;

	Tile* currentTile;

	bool shouldDestroy;
	bool isActive;

	Scene* scene;

	bool open;

	aie::Texture* texture;
	glm::vec4 uv;
};
#endif // !GameObject_H
