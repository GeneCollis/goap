#include "Random.h"

Random* Random::instance;

void Random::Create()
{
	instance = new Random();
}

void Random::Destroy()
{
	delete instance;
}

void Random::Seed(const unsigned int & a_seed)
{
	instance->mt = std::mt19937(a_seed);
}

void Random::SeedRandom()
{
	std::random_device rd;
	instance->mt = std::mt19937(rd());
}

// Returns a random number inclusive of min and max
int Random::Range(const int & a_min, const int & a_max)
{
	instance->iDist = std::move(std::uniform_int_distribution<int>(a_min, a_max));
	return instance->iDist(instance->mt);
}

// Returns a random number inclusive of min and max
float Random::Range(const float & a_min, const float & a_max)
{
	instance->fDist = std::move(std::uniform_real_distribution<float>(a_min, a_max));
	return instance->fDist(instance->mt);
}

// Returns a random number inclusive of min and max
double Random::Range(const double & a_min, const double & a_max)
{
	instance->dDist = std::move(std::uniform_real_distribution<double>(a_min, a_max));
	return instance->dDist(instance->mt);
}

Random::Random()
{
	std::random_device rd;
	mt = std::mt19937(rd());
}