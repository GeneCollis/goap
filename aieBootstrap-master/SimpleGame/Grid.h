#ifndef Grid_H
#define Grid_H
//#include "Renderer2D.h"
#include "GameObject.h"
#include "Random.h"

#include <glm/glm.hpp>

#include <functional>
#include <vector>
#include <list>
#include <map>

class Tile
{
public:
	glm::ivec2 pos = { 0, 0 };
	glm::vec2 worldPos = { 0, 0 };
	Tile* up = nullptr;
	Tile* right = nullptr;
	Tile* down = nullptr;
	Tile* left = nullptr;

	Grid* GetGrid() { return grid; }
	GameObject* GetGO() const { return go; }
	bool IsOpen() const { return open; }
	void SetOpen(bool a_open);
	bool IsDirty() const { return dirty; }

private:
	friend class Grid;
	friend class CompareFScore;

	Grid* grid = nullptr;

	GameObject* go = nullptr;
	bool open = true;

	glm::vec4 uv;
	bool dirty;

	bool goal = false;
	bool traversed = false;
	bool inQueue = false;
	float fScore = 0;
	float gScore = 0;
	float hScore = 0;
	Tile* prevTile = nullptr;

	float cost = 1.0f;
};

class Grid
{
public:
	Grid() = delete;
	Grid(const unsigned int a_width, const unsigned int a_height, const unsigned int a_size, const glm::vec2& a_worldPos = {0, 0});
	~Grid();

	// Returns the element at the index [x * height + y]
	Tile* Get(const int a_x, const int a_y) const
	{
		return IsInBounds(a_x, a_y) ? &gridData[a_x * height + a_y] : nullptr;
	}
	Tile* Get(const glm::ivec2& a_pos) const
	{
		return Get(a_pos.x, a_pos.y);
	}
	Tile* Get(const int& a_index) const
	{
		return IsInBounds(a_index) ? &gridData[a_index] : nullptr;
	}

	bool SetGO(const glm::ivec2 a_pos, GameObject* a_go);
	bool SetGO(const int a_index, GameObject* a_go);

	void SetOpen(Tile& a_tile, const bool a_open);

	// Draw a grid to the screen
	void Draw(aie::Renderer2D* a_renderer, const glm::vec2& a_mouseWorldPos, const bool a_debug, const float a_depth = (0.0f));
	void ImGui(Tile* a_mouseTile);

	std::vector<Tile*> CalculatePath(Tile* a_startNode, Tile* a_goalNode);

	std::vector<Tile*> CalculatePath(Tile* a_startNode, const std::vector<Tile*>& a_goalNodes);

	void CalculatePathStepStart(Tile* a_startNode, const std::vector<Tile*>& a_goalNodes);
	bool CalculatePathStep(std::vector<Tile*>& a_path);

	inline bool IsInBounds(const int& a_width, const int& a_height) const
	{
		return (a_width >= 0 && a_width < width) && (a_height >= 0 && a_height < height);
	}

	inline bool IsInBounds(const glm::ivec2 a_pos) const
	{
		return IsInBounds(a_pos.x, a_pos.y);
	}

	inline bool IsInBounds(const int& a_index) const
	{
		return (a_index >= 0 && a_index < width * height);
	}

	// Returns the pixel size of the tiles
	int Size() const { return size; }
	int Width() const { return width; }
	int Height() const { return height; }
	glm::vec2 WorldPos() const { return worldPos; }

	Tile* GetRandomAvailableTile() const
	{
		if (availableTiles.size() != 0)
		{
			auto item = availableTiles.begin();
			std::advance(item, Random::Range(0, int(availableTiles.size()) - 1));
			return item->second;
		}
		return nullptr;
	}
	
	void AddCallback(GameObject* a_go, std::function<void()> a_function)
	{
		if (a_go != nullptr)
			changedCallback.insert(std::pair<GameObject*, std::function<void()>>(a_go, a_function));
	}
	void RemoveCallback(GameObject* a_go)
	{
		if (a_go != nullptr)
			changedCallback.erase(changedCallback.find(a_go));
	}

private:
	// Called whenever the grid is changed
	void GridChanged();

	inline int PosToIndex(const glm::ivec2& a_pos)
	{
		return a_pos.x * height + a_pos.y;
	}

	inline glm::ivec2 IndexToPos(const int& a_index)
	{
		return {a_index / height, a_index % height};
	}

	// The pixel size of the grid
	int size;
	// The amount of tiles on the x & y axis
	int width;
	int height;
	// The data that the grid contains
	Tile* gridData;

	glm::vec2 worldPos;

	std::map<unsigned int, Tile*> availableTiles;

	std::map<GameObject*, std::function<void()>> changedCallback;

	Tile* pf_currentTile;
	Tile* pf_startTile;
	std::vector<Tile*> pf_goalTiles;
	std::list<Tile*> pf_queue;
	bool pf_reachedGoal;

	aie::Texture* floorTexture;
	glm::vec4 floorUV;
};
#endif // !Grid_H
