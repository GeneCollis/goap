#ifndef Wall_H
#define Wall_H
#include "GameObject.h"

class Wall : public GameObject
{
public:
	Wall(glm::vec2 a_pos, float a_size);

	virtual void Update(const float deltaTime, aie::Input* input) {};
	virtual void Draw(aie::Renderer2D* renderer2D, float depth = 0.0f, const bool& a_debug = false) const;
	virtual void ImGui() {};

	virtual ~Wall() {};
};
#endif // !Wall_H
