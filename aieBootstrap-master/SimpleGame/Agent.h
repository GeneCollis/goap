#ifndef Agent_H
#define Agent_H
#include "GameObject.h"
#include "Action.h"

#include <vector>

enum class Direction
{
	Up = 0,
	Right,
	Down,
	Left
};

class Key;
class Door;

class Agent : public GameObject
{
public:
	enum class State
	{
		Idle,
		Pathfinding,
		Moving,
		PickingUpKey,
		UnlockingDoor
	};

	Agent() = delete;
	Agent(const std::string a_name, Tile* a_tile, const float a_radius, const float a_speed, Grid* a_grid);
	virtual ~Agent() = default;

	virtual void Update(const float deltaTime, aie::Input* input);
	virtual void Draw(aie::Renderer2D* renderer2D, float depth = 0.0f, const bool& a_debug = false) const;
	virtual void ImGui();

	void SetPlan(const std::vector<GOAP::Action> newPlan);

	bool GetPlanComplete() const { return planComplete; }

	void Reset();

private:
	// Moves to a position
	bool GoTo(Tile* a_target);

	// Picks up the key if it is nearby
	bool PickUpKey(GameObject* key);

	// Unlocks the door if it has a key and the door is nearby
	bool UnlockDoor(Door* door);

	// Returns the state of this agent
	State GetState() const { return state; }

	// Moves to the target position
	void Movement(const float &deltaTime);
	void WalkAnimation(const float &deltaTime);

	void Look(const glm::vec2& a_direction);

	void GridChanged();

	// The movement speed of this agent
	float speed;
	// If this agent has the key or not
	bool hasKey;

	Direction facingDir;
	int animationFrame;
	int animationFrameCount;
	float animationTimer;
	float animationFPS;

	// The Graph this agent exists on
	Grid* grid;
	// The path this agent is following
	std::vector<Tile*> path;
	// The tile this agent is moving to
	Tile* targetTile;
	// The tile this agent is path finding to
	Tile* goalTile;

	bool hasPath;

	// The state of this agent
	State state;
	// This agents GOAP plan
	std::vector<GOAP::Action> plan;
	// The position in the plan that this agent is at
	int planIndex;
	bool planComplete;

	float pathfindingStepSpeed;
	float pathfindingTimer;
	bool pathfindingStep;
	bool pathfindingStarted;
};
#endif // !Agent_H
