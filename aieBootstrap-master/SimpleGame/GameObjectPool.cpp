#include "GameObjectPool.h"
#include "GameObject.h"

#include <algorithm>

GameObjectPool::GameObjectPool(const int a_size)
{
	pool = std::vector<GameObject*>();
	pool.reserve(a_size);
}

GameObject * GameObjectPool::Get()
{
	auto it = std::find_if(pool.begin(), pool.end(), [](const GameObject* go) -> bool { return go->IsActive() == false; });

	if (it != pool.end())
	{
		(*it)->SetActive(true);
		return *it;
	}

	return nullptr;
}

void GameObjectPool::Add(GameObject * a_gameObject)
{
	assert(a_gameObject != nullptr);

	pool.emplace_back(a_gameObject);
}

void GameObjectPool::DestroyAll()
{
	for (auto& go : pool)
		go->Destroy();
}
