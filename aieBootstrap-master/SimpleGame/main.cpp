#include "SimpleGameApp.h"

int Run();

// TODO remove this for distributable build
#ifdef _DEBUG
#include <vld.h>
int main()
{
	return Run();
}
#else
int WinMain()
{
	return Run();
}
#endif

int Run()
{
	auto app = new SimpleGameApp();
	app->run("Gene Collis - GOAP", 1280, 720, false);
	delete app;

	return 0;
}