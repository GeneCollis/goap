#pragma once
#include <glm/glm.hpp>
// std
#include <map>
namespace aie
{
	class Texture;
}
class TextureMap
{
public:
	static void Create();
	static void Destroy();

	enum TileKey : unsigned int
	{
		//Wall_0000 = 0,
		//Wall_0001 = 1,
		Up_Bit = 1,
		//Wall_0010 = 2,
		Right_Bit = 2,
		//Wall_0011,
		//Wall_0100 = 4,
		Down_Bit = 4,
		//Wall_0101,
		//Wall_0110,
		//Wall_0111,
		//Wall_1000 = 8,
		Left_Bit = 8,
		//Wall_1001,
		//Wall_1010,
		//Wall_1011,
		//Wall_1100,
		//Wall_1101,
		//Wall_1110,
		//Wall_1111,
		Wall = 15,
		Floor = 16,
		Door = 17,
		Key = 18,
		Player = 24,
	};

	static glm::vec4 GetUV(TileKey a_key, int a_animState = 0);
	static aie::Texture* Texture() { return instance->atlas; };
	static int TileSize() { return instance->tileSize; }

private:
	TextureMap();
	~TextureMap();

	static TextureMap* instance;

	const char* texfile = "../bin/textures/simple_tile.png";
	int tileSize = 32;
	int width;
	int height;
	aie::Texture* atlas;
};

