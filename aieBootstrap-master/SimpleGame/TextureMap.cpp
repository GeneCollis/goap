#include "TextureMap.h"

// Bootstrap
#include "Texture.h"

// std
#include <string>
#include <assert.h>

TextureMap* TextureMap::instance;

void TextureMap::Create()
{
	instance = new TextureMap();
}

void TextureMap::Destroy()
{
	delete instance;
}

TextureMap::TextureMap()
{
	atlas = new aie::Texture(texfile);

	width = atlas->getWidth() / tileSize;
	height = atlas->getHeight() / tileSize;

	//map = std::map<TileKey, aie::Texture*>();
}

TextureMap::~TextureMap()
{
	delete atlas;

	//// Delete all loaded textures
	//for (auto const& tex : map)
	//	delete tex.second;
}

glm::vec4 TextureMap::GetUV(TileKey a_key, int a_animState)
{
	return 
	{
		int((int(a_key) + a_animState) % instance->width) / float(instance->width),
		((int(a_key) + a_animState) / instance->width) / float(instance->height),
		1.0f / instance->width,
		1.0f / instance->height
	};
}

//{
//	// Search for the texture, and return it
//	{
//		auto it = instance->map.find(a_key);
//		if (it != instance->map.end())
//			return (*it).second;
//	}
//
//	// Get the file name and load it
//	aie::Texture* tex = new aie::Texture();
//	std::string file = instance->texfile;
//
//	switch (a_key)
//	{
//	default:
//		file.append(std::to_string(a_key)).append(".png");
//		break;
//	}
//
//	bool success = tex->load(&file[0]);
//	assert(success);
//	instance->map.insert({ a_key, tex });
//	return tex;
//}
