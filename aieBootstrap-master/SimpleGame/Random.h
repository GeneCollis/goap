#ifndef Random_H
#define Random_H
#include <random>

class Random
{
public:
	// Creates the random instance. Only call this once
	static void Create();
	// Destroys the random instance. Only call this after Create()
	static void Destroy();

	// Set a specific seed for the PRNG
	static void Seed(const unsigned int& a_seed);

	static void SeedRandom();

	static int Range(const int& a_min, const int& a_max);

	static float Range(const float& a_min, const float& a_max);

	static double Range(const double& a_min, const double& a_max);

private:
	Random();
	~Random() = default;

	static Random* Random::instance;

	std::mt19937 mt;
	std::uniform_int_distribution<int> iDist;
	std::uniform_real_distribution<float> fDist;
	std::uniform_real_distribution<double> dDist;
};
#endif // !Random_H
