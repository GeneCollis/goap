#include "SimpleGameApp.h"
#include "Key.h"
#include "Door.h"
#include "Wall.h"
#include "Agent.h"
#include "Scene.h"
#include "Grid.h"
#include "TextureMap.h"
#include "Random.h"
#include "MapGenerator.h"

// Bootstrap
#include "Renderer2D.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"

// dependencies
#include <imgui.h>

// std
#include <map>
#include <algorithm>
#include <functional>
#include <random>
#include <iostream>

SimpleGameApp::SimpleGameApp()
{
	state = State::Default;
	cameraPos = { 0, 0 };
	cameraTargetPos = cameraPos;
	cameraZoom = 0.5f;
	cameraZoomTarget = 1.0f;
	scrollState = 0;
	prevMouseScroll = 0;
	mouseWorldPos = { 0, 0 };
	followPlayer = false;
	seed = 0;
	autoRandomSeed = true;
	autoGenerate = true;
}

SimpleGameApp::~SimpleGameApp()
{

}

bool SimpleGameApp::startup()
{
	// Setup the random instance
	Random::Create();
	TextureMap::Create();

	RandomSeed();

	// Create our renderer
	renderer2D = new aie::Renderer2D();

	// Load the font
	font = new aie::Font("./font/consolas.ttf", 32);

	gameScene = new Scene();

	int width = getWindowWidth();
	int height = getWindowHeight();
	int pixelSize = 32;
	grid = new Grid(40, 23, pixelSize, { -(width >> 1), -(height >> 1) });

	// Create a Agent
	agent = new Agent("Mr. Smith", grid->Get(0, 0), 5.0f, 100.0f, grid);
	agent->SetTexture(TextureMap::Texture());
	agent->SetUV(TextureMap::GetUV(TextureMap::Player));
	gameScene->AddGameObject(agent);

	player = nullptr;
	key = new Key(grid->Get(3, 2), 2.0f);
	key->SetTexture(TextureMap::Texture());
	key->SetUV(TextureMap::GetUV(TextureMap::Key));
	gameScene->AddGameObject(key);
	door = new Door(grid->Get(2, 1), false, pixelSize);
	door->SetTexture(TextureMap::Texture());
	door->SetUV(TextureMap::GetUV(TextureMap::Door));
	gameScene->AddGameObject(door);

	grid->AddCallback(static_cast<GameObject*>(door), std::bind(&SimpleGameApp::GridChanged, this));

	wallPool = GameObjectPool(80 * 45 + 100);
	Wall* wall;
	for (size_t i = 0; i < (80 * 45 + 100); i++)
	{
		wall = new Wall(glm::vec2{ -100 - 100 }, float(pixelSize));
		wall->SetActive(false);
		wallPool.Add(wall);
		gameScene->AddGameObject(wall);
	}

	tileUnderMouse = nullptr;

	// GOAP SYSTEM SETUP
#pragma region GOAP setup

	// Enum for the players location
	enum location
	{
		key,
		door,
		none
	};

	// Create the world state
	worldState = GOAP::WorldState();
	worldState.Add<bool>("playerHasKey", false);
	// Add a location worldValue to the worldState
	worldState.Add("location", location::none);
	worldState.Add("doorLocked", true);

	// Create the goal state
	goalState = GOAP::WorldState();
	goalState.Add<bool>("doorLocked", false); // Add values to the goal state

	// Create a planner
	planner = GOAP::Planner();

#pragma region Actions

	// Create a list of actions
	actions = std::vector<GOAP::Action>();

	// Add some actions

	// Pick up key action
	GOAP::Action pickUpKey("pickUpKey");
	pickUpKey.AddPrecondition<bool>("playerHasKey", false);
	pickUpKey.AddPrecondition("location", location::key);
	pickUpKey.AddEffect<bool>("playerHasKey", true);
	actions.emplace_back(std::move(pickUpKey));

	// Go to key action
	GOAP::Action goToKey("goToKey");
	goToKey.AddInversePrecondition("location", location::key);
	goToKey.AddEffect("location", location::key);
	actions.emplace_back(std::move(goToKey));

	// Go to door action
	GOAP::Action goToDoor("goToDoor");
	goToDoor.AddInversePrecondition("location", location::door);
	goToDoor.AddEffect("location", location::door);
	actions.emplace_back(std::move(goToDoor));

	// Unlock door action
	GOAP::Action unlockDoor("unlockDoor");
	unlockDoor.AddPrecondition("doorLocked", true);
	unlockDoor.AddPrecondition("location", location::door);
	unlockDoor.AddPrecondition("playerHasKey", true);
	unlockDoor.AddEffect("doorLocked", false);
	actions.emplace_back(std::move(unlockDoor));

#pragma endregion

#pragma endregion

	Restart();

	return true;
}

void SimpleGameApp::shutdown()
{
	Random::Destroy();
	TextureMap::Destroy();

	grid->RemoveCallback(static_cast<GameObject*>(door));

	delete font;
	delete renderer2D;

	delete gameScene;
	delete grid;
}

void SimpleGameApp::update(float deltaTime)
{
	// Get the Input instance
	aie::Input* input = aie::Input::getInstance();
	// Update the scroll state
	scrollState = (int)(input->getMouseScroll() - prevMouseScroll);
	CameraZoom(deltaTime);
	CameraMovement(deltaTime);
	// Update the game scene
	gameScene->Update(deltaTime, input);
	// Display the imGui window
	ImGuiWindow();

	// If we press escape exit the application
	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();

	prevMouseScroll = input->getMouseScroll();

	glm::vec2 camPos;
	renderer2D->getCameraPos(camPos.x, camPos.y);
	mouseScreenPos = { input->getMouseX(), input->getMouseY() };
	mouseWorldPos = (mouseScreenPos - glm::vec2{ getWindowWidth(), getWindowHeight() } / 2.0f) / cameraZoom + camPos;

	if (agent->GetPlanComplete() && (seedFailed == false || autoGenerate))
	{
		Restart();
	}

	ImGuiIO io = ImGui::GetIO();
	glm::ivec2 pos = (mouseWorldPos - grid->WorldPos()) / float(grid->Size());
	if (io.WantCaptureMouse == false)
	{
		if (grid->IsInBounds(pos))
		{

			tileUnderMouse = grid->Get(pos);
			if (input->isMouseButtonDown(0))
			{
				if (tileUnderMouse->GetGO() == nullptr)
				{
					Wall* newWall = (Wall*)wallPool.Get();
					newWall->SetCurrentTile(tileUnderMouse);
				}
			}
			if (input->isMouseButtonDown(1))
			{
				if (grid->IsInBounds(pos))
				{
					if (tileUnderMouse->GetGO() != nullptr && tileUnderMouse->GetGO()->GetName() == "Wall")
					{
						tileUnderMouse->GetGO()->SetActive(false);
						tileUnderMouse->GetGO()->SetCurrentTile(nullptr);
					}
				}
			}
		}

		if (input->wasMouseButtonPressed(2))
		{
			mouseMiddleClickPos = mouseScreenPos;
			middleClickCameraPos = cameraPos;
		}
		if (input->isMouseButtonDown(2))
		{
			cameraTargetPos = middleClickCameraPos + (mouseMiddleClickPos - mouseScreenPos) * (1 / cameraZoom);

			float halfWidth = (float)(getWindowWidth() >> 1);
			float halfHeight = (float)(getWindowHeight() >> 1);
			cameraPos.x = glm::clamp(cameraPos.x, -halfWidth, halfWidth);
			cameraPos.y = glm::clamp(cameraPos.y, -halfHeight, halfHeight);
		}
	}
	else
	{
		tileUnderMouse = nullptr;
	}

	if (input->wasKeyPressed(aie::INPUT_KEY_SPACE))
	{
		ResetCamera();
	}
}

void SimpleGameApp::draw()
{
	// Wipe the screen to the background colour
	clearScreen();

	// set the camera position before we begin rendering
	renderer2D->setCameraPos(cameraPos.x, cameraPos.y);

	renderer2D->setCameraZoom(cameraZoom);

	// Begin drawing sprites
	renderer2D->begin();

	renderer2D->setRenderColour(1.0f, 1.0f, 1.0f);

	grid->Draw(renderer2D, mouseWorldPos, gameScene->GetShowDebug(), -1.0f);

	gameScene->Draw(renderer2D);

	//// Setup some variables
	//const float halfHeight = float(getWindowHeight() >> 1);
	//const float halfWidth = float(getWindowWidth() >> 1);

	//// Set the text colour
	//renderer2D->setRenderColour(1.0f, 1.0f, 1.0f);

	//// Output some text, uses the last used colour
	//renderer2D->drawText(font, "ESC to quit", -halfWidth, -halfHeight + 8.0f);

	// Done drawing
	renderer2D->end();
}

void SimpleGameApp::CameraZoom(const float& a_deltaTime)
{
	cameraZoomTarget += (float)scrollState * (0.1f);
	if (cameraZoom != cameraZoomTarget)
	{
		cameraZoomTarget = glm::clamp(cameraZoomTarget, 0.2f, 6.0f);
		float difference = cameraZoomTarget - cameraZoom;
		if (std::abs(difference) < 0.001f)
		{
			cameraZoom = cameraZoomTarget;
			difference = 0;
		}
		cameraZoom += difference * a_deltaTime * 10.0f;
	}
}

void SimpleGameApp::CameraMovement(const float & a_deltaTime)
{
	if (followPlayer)
	{
		cameraTargetPos = agent->GetPosition();
	}

	if (cameraPos != cameraTargetPos)
	{
		glm::vec2 vecBetween = cameraTargetPos - cameraPos;
		float distance = glm::length(vecBetween);
		if (distance < 0.001f)
		{
			cameraPos = cameraTargetPos;
			distance = 0;
		}
		cameraPos += glm::normalize(vecBetween) * distance * a_deltaTime * 10.0f;
	}
}

void SimpleGameApp::ImGuiWindow()
{
	gameScene->ImGui();

	ImGui::Begin("Info");
	ImGuiTreeNodeFlags defaultOpen = ImGuiTreeNodeFlags_DefaultOpen;
	if (ImGui::CollapsingHeader("Controls", defaultOpen))
	{
		ImGui::Text("LMB - Add Wall");
		ImGui::Text("RMB - Remove Wall");
		ImGui::Text("Middle Click - Pan around");
		ImGui::Text("Scroll - Zoom in and out");
		ImGui::Text("Space - Reset camera");
	}

	if (ImGui::CollapsingHeader("Debug"))
	{
		bool showDebug = gameScene->GetShowDebug();
		ImGui::Checkbox("Show Debug", &showDebug);
		gameScene->SetShowDebug(showDebug);
	}

	ImGui::End();

	if (gameScene->GetShowDebug())
	{
		ImGui::Begin("Debug");

		std::string s = "FPS: ";
		s.append(&std::to_string(m_fps)[0]);
		ImGui::Text(&s[0]);

		ImGui::Checkbox("Auto generate", &autoGenerate);
		if (autoGenerate == false && ImGui::Button("Generate"))
			Restart();
		ImGui::Checkbox("Auto random seed", &autoRandomSeed);
		if (autoRandomSeed == false && ImGui::Button("Randomize seed"))
			RandomSeed();
		ImGui::InputInt("Seed", &seed);
		seed = glm::clamp(seed, 0, 1000000);

		if (seedFailed)
		{
			ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "Seed Failed");
		}
		else
		{
			ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), "Seed Successful");
		}

		ImGui::Checkbox("Camera follow", &followPlayer);

		ImGui::End();

		grid->ImGui(tileUnderMouse);
	}

}

bool SimpleGameApp::GenerateMap()
{
	if (autoRandomSeed)
		RandomSeed();

	Random::Seed(seed);

	state = State::MapGeneration;

	MapGenerator mapGen = MapGenerator(grid->Width(), grid->Height(), 4, 3, 4, 3, 4, 3, 4);

	std::vector<MapGenerator::TileType> map = mapGen.GenerateMap();

	Tile * tile;
	GameObject * go;

	bool placedPlayer = false;
	bool placedDoor = false;
	bool placedKey = false;

	for (int i = 0; i < map.size(); i++)
	{
		tile = grid->Get(i);
		go = tile->GetGO();

		if (go != nullptr)
			go->SetActive(false);
	}

	for (int i = 0; i < map.size(); i++)
	{
		tile = grid->Get(i);
		go = tile->GetGO();

		if (go != nullptr)
			go->SetActive(false);

		switch (map[i])
		{
		case MapGenerator::TileType::Floor:
		{
			if (go != nullptr)
			{
				go->SetCurrentTile(nullptr);
			}
		}
		break;
		case MapGenerator::TileType::Wall:
		{
			Wall* wall = (Wall*)wallPool.Get();
			assert(wall != nullptr);
			wall->SetCurrentTile(tile);
		}
		break;
		case MapGenerator::TileType::Door:
		{
			door->SetActive(true);
			door->SetUpDown(false);
			door->SetCurrentTile(tile);
			door->Lock();

			placedDoor = true;
		}
		break;
		case MapGenerator::TileType::DoorUp:
		{
			door->SetActive(true);
			door->SetUpDown(true);
			door->SetCurrentTile(tile);
			door->Lock();

			placedDoor = true;
		}
		break;
		case MapGenerator::TileType::Player:
		{
			agent->SetActive(true);
			agent->SetCurrentTile(tile);

			placedPlayer = true;
		}
		break;
		case MapGenerator::TileType::Key:
		{
			key->SetActive(true);
			key->SetCurrentTile(tile);

			placedKey = true;
		}
		case MapGenerator::TileType::Goal:
		{
			//TODO set the GOAP goal to reach this tile
		}
		break;
		default:
			break;
		}

	}

	if (!(placedDoor && placedKey && placedPlayer))
	{
		return false;
	}

	state = State::Default;
	SetupWalls();
	return true;
}

void SimpleGameApp::GridChanged()
{
	SetupWalls();
}

void SimpleGameApp::SetupWalls()
{
	//// Don't setup walls if we're still generating the map
	//if (state == State::MapGeneration)
	//	return;

	Tile* tile;
	int bitFlag;
	for (int i = 0; i < (grid->Width() * grid->Height()); i++)
	{
		tile = grid->Get(i);

		if (tile->IsDirty() && tile->IsOpen() == false && tile->GetGO() != nullptr && tile->GetGO()->GetName() == "Wall")
		{
			bitFlag = 0;
			if (tile->up == nullptr || tile->up->IsOpen() == false)
				bitFlag += TextureMap::Up_Bit;
			if (tile->right == nullptr || tile->right->IsOpen() == false)
				bitFlag += TextureMap::Right_Bit;
			if (tile->down == nullptr || tile->down->IsOpen() == false)
				bitFlag += TextureMap::Down_Bit;
			if (tile->left == nullptr || tile->left->IsOpen() == false)
				bitFlag += TextureMap::Left_Bit;

			tile->GetGO()->SetUV(TextureMap::GetUV((TextureMap::TileKey)bitFlag));
		}
	}

}

void SimpleGameApp::Restart()
{
	door->SetCurrentTile(nullptr);
	key->SetCurrentTile(nullptr);
	agent->SetCurrentTile(nullptr);

	if (GenerateMap())
	{
		seedFailed = false;
		NewPlan();
	}
	else
	{
		seedFailed = true;
		//if (autoRandomSeed)
		//{
		//	while (seedFailed == true)
		//	{
		//		seedFailed = !GenerateMap();
		//	}
		//}
	}
}

void SimpleGameApp::RandomSeed()
{
	seed = Random::Range(0, 1000000);
}

void SimpleGameApp::NewPlan()
{
	agent->Reset();

	std::vector<GOAP::Action> plan;
	// Calculate a plan
	if (planner.CalculatePlan(worldState, goalState, actions, plan))
	{
		// Give the agent the plan
		agent->SetPlan(std::move(plan));
	}
}

void SimpleGameApp::ResetCamera()
{
	cameraZoomTarget = 1.0f;
	cameraTargetPos = { 0.0f, 0.0f };
	followPlayer = false;
}

