#ifndef Door_H
#define Door_H
#include "GameObject.h"

class Door : public GameObject
{
public:

	Door() : GameObject("Door"), isLocked(true), upDown(false) {};
	Door(Tile* a_tile, const bool& a_upDown = false, const float& a_size = 10.0f)
		: GameObject("Door", a_tile, a_size), isLocked(true), upDown(a_upDown)
	{
		open = false;
	};

	virtual void Update(const float deltaTime, aie::Input* input) {};
	virtual void Draw(aie::Renderer2D* renderer2D, float depth = 0.0f, const bool& a_debug = false) const;
	virtual void ImGui() {};

	bool IsLocked() const { return isLocked; }

	void Lock(); 
	void Unlock();

	void SetUpDown(const bool & a_upDown) { upDown = a_upDown; }

private:
	bool isLocked;
	bool upDown;
};

#endif // !Door_H
