#pragma once
#include "GameObject.h"

class Key;

class Player : public GameObject
{
public:
	Player() : GameObject("Player"), speed(10), hasKey(false) {};
	Player(const glm::vec2 a_position, const float a_radius, const float a_speed)
		: GameObject("Player", a_position, a_radius), speed(a_speed), hasKey(false) {};

	virtual void Update(const float deltaTime, aie::Input* input);
	virtual void Draw(aie::Renderer2D* renderer2D, float depth = 0.0f, const bool& a_debug = false) const;
	virtual void ImGui();

	void Movement(aie::Input * input, const float &deltaTime);

	bool PickupKey(Key* key);

	bool HasKey() const { return hasKey; };

private:
	float speed;
	bool hasKey;
};

