#include "Agent.h"
#include "Scene.h"
#include "Grid.h"
#include "TextureMap.h"

// GOAP
#include "Key.h"
#include "Door.h"
// bootstrap
#include "Renderer2D.h"
#include "Input.h"

#include <imgui.h>
#include <glm/gtc/constants.hpp>

Agent::Agent(const std::string a_name, Tile * a_tile, const float a_radius, const float a_speed, Grid * a_grid)
	: GameObject(a_name, a_tile, a_radius), speed(a_speed), grid(a_grid),
	hasKey(false), facingDir(Direction::Up), animationFrame(0), animationFPS(8), animationFrameCount(8),
	animationTimer(0), targetTile(nullptr), planIndex(0), state(State::Idle), pathfindingStep(false),
	pathfindingStarted(false), pathfindingStepSpeed(0.05f), planComplete(false)
{
	grid->AddCallback(static_cast<GameObject*>(this), std::bind(&Agent::GridChanged, this));
}

void Agent::Update(const float deltaTime, aie::Input * input)
{
	switch (state)
	{
	case Agent::State::Idle:

		animationFrame = 0;

		// Choose new action to do, If we have a plan
		if (planIndex < plan.size())
		{
			if (plan.at(planIndex).GetName() == "pickUpKey")
			{
				if (PickUpKey(scene->GetNearbyGameObject("Key", position)))
				{
					++planIndex;
				}
			}
			else if (plan.at(planIndex).GetName() == "goToKey")
			{
				GameObject* key = scene->GetNearbyGameObject("Key", position);
				assert(key != nullptr);
				Tile* keyTile = key->GetCurrentTile();
				assert(keyTile != nullptr);
				GoTo(keyTile);
			}
			else if (plan.at(planIndex).GetName() == "goToDoor")
			{
				GameObject* door = scene->GetNearbyGameObject("Door", position);
				assert(door != nullptr);
				Tile* doorTile = door->GetCurrentTile();
				assert(doorTile != nullptr);
				GoTo(doorTile);
			}
			else if (plan.at(planIndex).GetName() == "unlockDoor")
			{
				if (UnlockDoor((Door*)scene->GetNearbyGameObject("Door", position)))
				{
					++planIndex;
				}
			}
		}
		else
		{
			planComplete = true;
		}
		break;
	case Agent::State::Pathfinding:
		pathfindingTimer += deltaTime;
		if (pathfindingTimer > pathfindingStepSpeed)
		{
			pathfindingTimer -= pathfindingStepSpeed;
			bool finished = false;
			do
			{
				if (grid->CalculatePathStep(path))
				{
					finished = true;
					if (path.size() > 0)
					{
						targetTile = path[path.size() - 1];
						hasPath = true;
						state = State::Moving;
					}
				}
			} while (pathfindingStep == false && finished == false);
		}
		break;
	case Agent::State::Moving:
		Movement(deltaTime);
		break;
	case Agent::State::PickingUpKey:

		break;
	case Agent::State::UnlockingDoor:

		break;
	default:
		break;
	}
}

void Agent::Draw(aie::Renderer2D * a_renderer2D, float a_depth, const bool& a_debug) const
{
	if (texture != nullptr)
	{
		a_renderer2D->setRenderColour(1.0f, 1.0f, 1.0f, 1.0f);
		a_renderer2D->setUVRect(TextureMap::GetUV(TextureMap::Player, animationFrame));
		a_renderer2D->drawSprite(texture, position.x, position.y, TextureMap::TileSize(), TextureMap::TileSize(), int(facingDir) * -glm::half_pi<float>(), a_depth);
	}
	else
	{
		// Body
		a_renderer2D->setRenderColour(1.0f, 1.0f, 0.0f, 1.0f);
		a_renderer2D->drawCircle(position.x, position.y, radius, a_depth);
		// Eyes
		a_renderer2D->setRenderColour(1.0f, 1.0f, 1.0f, 1.0f);
		a_renderer2D->drawCircle(position.x + radius / 2, position.y + radius / 2, radius / 4, a_depth);
		a_renderer2D->drawCircle(position.x - radius / 2, position.y + radius / 2, radius / 4, a_depth);
		// Pupils
		a_renderer2D->setRenderColour(0.0f, 0.0f, 0.0f, 1.0f);
		a_renderer2D->drawCircle(position.x + radius / 2, position.y + radius / 2, radius / 6, a_depth);
		a_renderer2D->drawCircle(position.x - radius / 2, position.y + radius / 2, radius / 6, a_depth);
	}

	if (a_debug)
	{
		// Direction line
		a_renderer2D->setRenderColour(1.0f, 0.0f, 0.0f, 1.0f);
		if (targetTile != nullptr)
			a_renderer2D->drawLine(position.x, position.y, targetTile->worldPos.x, targetTile->worldPos.y, 2, a_depth - 0.01f);
		// Path
		a_renderer2D->setRenderColour(0.0f, 0.0f, 1.0f, 1.0f);
		if (path.size() > 1)
			for (size_t i = 0; i < path.size() - 1; i++)
				a_renderer2D->drawLine(path[i]->worldPos.x, path[i]->worldPos.y, path[i + 1]->worldPos.x, path[i + 1]->worldPos.y, 2, a_depth - 0.01f);
	}

}

void Agent::ImGui()
{
	ImGui::Begin("Agent");

	ImGui::Checkbox("Slow A*", &pathfindingStep);
	if (pathfindingStep)
	{
		ImGui::Text("A* Speed:");
		ImGui::SliderFloat(" ", &pathfindingStepSpeed, 0.0f, 1.0f, "%.2f");
	}

	ImGui::Checkbox("Has Key", &hasKey);

	ImGui::Text("Speed:");
	ImGui::SliderFloat("  ", &speed, 0, 55500, "%.0f");

	switch (state)
	{
	case Agent::State::Idle:
		ImGui::Text("State: Idle");
		break;
	case Agent::State::Pathfinding:
		ImGui::Text("State: Pathfinding");
		break;
	case Agent::State::Moving:
		ImGui::Text("State: Moving");
		break;
	case Agent::State::PickingUpKey:
		ImGui::Text("State: PickingUpKey");
		break;
	case Agent::State::UnlockingDoor:
		ImGui::Text("State: UnlockingDoor");
		break;
	default:
		break;
	}

	//-------------

	ImGui::Text("GOAP Plan:");
	// Calculate a plan
	if (plan.size() == 0)
		ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "No plan.");
	else
	{
		unsigned int i = 1;
		for (auto action : plan)
			if (i == planIndex)
				ImGui::TextColored(ImVec4(0.1f, 0.5f, 0.9f, 1.0f), &(std::to_string(i++)).append(". ").append(action.GetName())[0]);
			else
				ImGui::Text(&(std::to_string(i++)).append(". ").append(action.GetName())[0]);
	}

	ImGui::End();
}

void Agent::SetPlan(const std::vector<GOAP::Action> newPlan)
{
	plan = newPlan;
}

void Agent::Reset()
{
	hasKey = false;
	path.clear();
	targetTile = nullptr;
	goalTile = nullptr;
	hasPath = false;
	state = State::Idle;
	plan.clear();
	planIndex = 0;
	planComplete = false;
	pathfindingStarted = false;
	animationTimer = 0;
	animationFrame = 0;
}

void Agent::Movement(const float &deltaTime)
{
	glm::vec2 direction = targetTile->worldPos - position;

	// Normalize if direction isn't a zero vector
	if (direction != glm::vec2{ 0, 0 })
		direction = glm::normalize(direction);

	Look(direction);

	float dist = glm::distance(position, targetTile->worldPos);
	if (dist < speed * deltaTime)
	{
		SetCurrentTile(targetTile);

		if (path.size() > 0)
		{
			path.pop_back();
			if (path.size() > 0)
				targetTile = path[path.size() - 1];
		}

		if (currentTile == goalTile
			|| currentTile == goalTile->up || currentTile == goalTile->down
			|| currentTile == goalTile->left || currentTile == goalTile->right)
		{
			targetTile = nullptr;
			goalTile = nullptr;
			hasPath = false;
			state = State::Idle;
			++planIndex;
		}
	}
	else
	{
		WalkAnimation(deltaTime);
		position += direction * speed * deltaTime;
	}

}

void Agent::WalkAnimation(const float & deltaTime)
{
	animationTimer += deltaTime;
	if (animationTimer > 1.0f / animationFPS)
	{
		++animationFrame;
		animationFrame %= animationFrameCount;
		animationTimer -= 1.0f / animationFPS;
	}

}

void Agent::Look(const glm::vec2 & a_direction)
{
	if (a_direction.x > 0)
		facingDir = Direction::Right;
	else if (a_direction.x < 0)
		facingDir = Direction::Left;
	if (a_direction.y > 0)
		facingDir = Direction::Up;
	else if (a_direction.y < 0)
		facingDir = Direction::Down;
}

void Agent::GridChanged()
{
	if (state == State::Moving || state == State::Pathfinding)
	{
		if (targetTile != nullptr && targetTile->IsOpen() == false)
		{
			targetTile = currentTile;
		}
		GoTo(goalTile);
	}
}

bool Agent::GoTo(Tile* a_target)
{
	path.clear();
	goalTile = a_target;

	Tile* startTile = state == State::Moving ? targetTile : currentTile;

	if (startTile == nullptr)
	{
		return false;
	}

	std::vector<Tile*> goalTiles = { a_target, a_target->up, a_target->down, a_target->left, a_target->right };

	for (auto it = goalTiles.begin(); it != goalTiles.end(); )
	{
		if (*it == nullptr || (*it)->IsOpen() == false)
		{
			it = goalTiles.erase(it);
		}
		else
		{
			it++;
		}
	}

	if (goalTiles.size() > 0)
	{
		if (pathfindingStep)
		{
			grid->CalculatePathStepStart(startTile, goalTiles);
			pathfindingStarted = true;
			state = State::Pathfinding;
			pathfindingTimer = 0;
			return true;
		}
		else
			path = grid->CalculatePath(startTile, goalTiles);
	}

	if (path.size() > 0)
	{
		targetTile = path[path.size() - 1];
		state = State::Moving;
		hasPath = true;
		return true;
	}
	else
	{
		hasPath = false;
		return false;
	}
}

bool Agent::PickUpKey(GameObject * key)
{
	// Make sure we aren't doing other things
	if (state != State::Idle)
		return false; // Failed

	state = State::PickingUpKey;

	Look(glm::normalize(key->GetPosition() - position));

	hasKey = true;
	key->SetActive(false);
	key->SetCurrentTile(nullptr);
	state = State::Idle;
	return true;
}

bool Agent::UnlockDoor(Door * door)
{
	// Make sure we aren't doing other things
	if (state != State::Idle)
		return false; // Failed

	// If this agent doesn't have the key
	if (!hasKey)
		return false;

	state = State::UnlockingDoor;

	Look(glm::normalize(door->GetPosition() - position));

	// Unlock the door
	door->Unlock();
	state = State::Idle;
	return true;
}

