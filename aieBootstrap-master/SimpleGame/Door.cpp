#include "Door.h"
#include "Grid.h"
#include "TextureMap.h"

#include "Renderer2D.h"

void Door::Draw(aie::Renderer2D * a_renderer2D, float a_depth, const bool& a_debug) const
{
	if (texture != nullptr)
	{
		if (isLocked)
		{
			a_renderer2D->setRenderColour(1.0f, 1.0f, 1.0f, 1.0f);
			a_renderer2D->setUVRect(uv);
			a_renderer2D->drawSprite(texture, position.x, position.y, TextureMap::TileSize(), TextureMap::TileSize(), 0.0f, a_depth);
		}
	}
	else
	{
		a_renderer2D->setRenderColour(0.2f, 0.2f, 0.2f, 1.0f);
		if (isLocked)
		{
			a_renderer2D->drawBox(position.x, position.y, upDown ? (radius / 4) : radius, upDown ? radius : (radius / 4), 0.0f, a_depth);
		}
		else
		{
			if (upDown)
			{
				a_renderer2D->drawBox(position.x, position.y + (radius * 3) / 8, radius / 4, radius / 4, 0.0f, a_depth);
				a_renderer2D->drawBox(position.x, position.y - (radius * 3) / 8, radius / 4, radius / 4, 0.0f, a_depth);
			}
			else
			{
				a_renderer2D->drawBox(position.x + (radius * 3) / 8, position.y, radius / 4, radius / 4, 0.0f, a_depth);
				a_renderer2D->drawBox(position.x - (radius * 3) / 8, position.y, radius / 4, radius / 4, 0.0f, a_depth);
			}
		}
	}
}

void Door::Lock()
{
	isLocked = true;
	if (currentTile != nullptr)
	{
		currentTile->SetOpen(!isLocked);
	}
}

void Door::Unlock()
{
	isLocked = false;
	if (currentTile != nullptr)
	{
		currentTile->SetOpen(!isLocked);
	}
}
