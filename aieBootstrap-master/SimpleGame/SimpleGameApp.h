#pragma once
#include "Application.h"
#include "GameObjectPool.h"

// GOAP
#include "Action.h"
// std
#include <vector>

#include <glm/glm.hpp>

class GameObject;
class Player;
class Key;
class Door;
class Agent;
class Scene;
class Grid;
class Tile;

namespace aie
{
	class Renderer2D;
	class Font;
	class Texture;
}

// GOAP
#include "Planner.h"

class SimpleGameApp : public aie::Application
{
public:

	SimpleGameApp();
	virtual ~SimpleGameApp();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();


protected:

	enum class State
	{
		Default,
		MapGeneration
	};

	void ImGuiWindow();

	void CameraZoom(const float& a_deltaTime);
	void CameraMovement(const float& a_deltaTime);

	bool GenerateMap();
	void GridChanged();
	void SetupWalls();

	void Restart();
	void RandomSeed();
	void NewPlan();

	void ResetCamera();
	
	State state;

	aie::Renderer2D*	renderer2D;
	aie::Font*			font;

	Player* player;
	Key* key;
	Door* door;
	Agent* agent;

	Scene* gameScene;

	int seed;
	bool seedFailed;
	bool autoRandomSeed;
	bool autoGenerate;

	// GOAP
	GOAP::Planner planner;
	GOAP::WorldState worldState;
	GOAP::WorldState goalState;
	std::vector<GOAP::Action> actions;

	glm::vec2 cameraPos;
	glm::vec2 cameraTargetPos;
	glm::vec2 middleClickCameraPos;
	float cameraZoom;
	float cameraZoomTarget;
	bool followPlayer;

	int scrollState;
	double prevMouseScroll;

	Grid* grid;

	glm::vec2 mouseWorldPos;
	glm::vec2 mouseMiddleClickPos;
	glm::vec2 mouseScreenPos;

	Tile* tileUnderMouse;

	GameObjectPool wallPool;
};