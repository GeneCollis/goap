#include "MapGenerator.h"

std::vector<MapGenerator::TileType> MapGenerator::GenerateMap()
{
	std::vector<Room> rooms = std::vector<Room>();
	rooms.resize(roomCount);

	std::vector<Corridor> corridors = std::vector<Corridor>();
	corridors.resize(roomCount - 1);

	// Setup all of the rooms and corridors
	rooms[0] = SetupRoomCentre();
	corridors[0] = SetupCorridor(rooms[0], true);

	for (size_t i = 1; i < roomCount; i++)
	{
		rooms[i] = SetupRoom(corridors[i - 1]);

		if (i < roomCount - 1)
		{
			corridors[i] = SetupCorridor(rooms[i]);
		}
	}

	assert(roomCount > 1);
	// Choose where to put the key and door
	keyRoom = Random::Range(0, roomCount - 2);
	doorCorridor = Random::Range(keyRoom, roomCount - 1);

	std::vector<TileType> tiles = std::vector<TileType>();
	tiles.resize(width * height);

	for (size_t i = 0; i < tiles.size(); i++)
		tiles[i] = Wall;

	SetTilesForRoom(rooms[0], tiles, true, keyRoom == 0);
	SetTilesForCorridor(corridors[0], tiles, doorCorridor == 0);

	for (size_t i = 1; i < roomCount; i++)
	{
		SetTilesForRoom(rooms[i], tiles, false, keyRoom == i);
		if (i < roomCount - 1)
		{
			SetTilesForCorridor(corridors[i], tiles, doorCorridor == i);
		}
	}

	return std::move(tiles);
}

inline MapGenerator::Room MapGenerator::SetupRoomCentre()
{
	int w = roomWidth.Random();
	int h = roomHeight.Random();
	Room room = { int(width / 2.0f - w / 2.0f), int(height / 2.0f - h / 2.0f), std::move(w), std::move(h) };

	return std::move(room);
}

inline MapGenerator::Room MapGenerator::SetupRoom(const Corridor & a_corridoor)
{
	Room room = Room();

	room.enteringCorridor = a_corridoor.direction;

	room.width = roomWidth.Random();
	room.height = roomHeight.Random();

	int endY = a_corridoor.EndY();
	int endX = a_corridoor.EndX();

	switch (a_corridoor.direction)
	{
	case MapGenerator::Up:
		if (room.height > height - endY)
			room.height = height - endY;
		room.y = endY;

		room.x = Random::Range(endX - room.width + 1, endX);
		if (room.x > width - room.width)
			room.x = width - room.width;
		if (room.x < 0)
			room.x = 0;
		break;
	case MapGenerator::Right:
		if (room.width > width - endX)
			room.width = width - endX;
		room.x = endX;

		room.y = Random::Range(endY - room.height + 1, endY);
		if (room.y > height - room.height)
			room.y = height - room.height;
		if (room.y < 0)
			room.y = 0;
		break;
	case MapGenerator::Down:
		if (room.height > endY)
			room.height = endY;
		room.y = endY - room.height + 1;

		room.x = Random::Range(endX - room.width + 1, endX);
		if (room.x > width - room.width)
			room.x = width - room.width;
		if (room.x < 0)
			room.x = 0;
		break;
	case MapGenerator::Left:
		if (room.width > endX)
			room.width = endX;
		room.x = endX - room.width + 1;

		room.y = Random::Range(endY - room.height + 1, endY);
		if (room.y > height - room.height)
			room.y = height - room.height;
		if (room.y < 0)
			room.y = 0;
		break;
	default:
		break;
	}

	return std::move(room);
}

inline MapGenerator::Corridor MapGenerator::SetupCorridor(const Room & a_room, const bool& a_first)
{
	Corridor corridor;

	corridor.direction = (Direction)Random::Range(0, 3);

	if (a_first == false)
	{
		Direction oppositeDirection = (Direction)((a_room.enteringCorridor + 2) % 4);

		if (corridor.direction == oppositeDirection)
		{
			corridor.direction = (Direction)((corridor.direction + 1) % 4);
		}
	}

	corridor.length = corridorLength.Random();

	int maxLength = corridorLength.max;
	switch (corridor.direction)
	{
	case MapGenerator::Up:
		corridor.x = Random::Range(0, (a_room.width - 1)) + a_room.x;
		corridor.y = a_room.y + a_room.height;

		maxLength = height - corridor.y - roomHeight.min;
		break;
	case MapGenerator::Right:
		corridor.x = a_room.x + a_room.width;
		corridor.y = Random::Range(0, (a_room.height - 1)) + a_room.y;

		maxLength = width - corridor.x - roomWidth.min;
		break;
	case MapGenerator::Down:
		corridor.x = Random::Range(0, a_room.width - 1) + a_room.x;
		corridor.y = a_room.y - 1;

		maxLength = corridor.y - roomHeight.min;
		break;
	case MapGenerator::Left:
		corridor.x = a_room.x - 1;
		corridor.y = Random::Range(0, a_room.height - 1) + a_room.y;

		maxLength = corridor.x - roomWidth.min;
		break;
	default:
		break;
	}

	if (corridor.length > maxLength)
	{
		corridor.length = maxLength;
	}

	return std::move(corridor);
}

inline void MapGenerator::SetTilesForRoom(const Room & a_room, std::vector<TileType>& a_tiles, bool a_addPlayer, bool a_addKey, bool a_addGoal)
{
	for (int j = 0; j < a_room.width; j++)
	{
		int xCoord = a_room.x + j;

		for (int k = 0; k < a_room.height; k++)
		{
			int yCoord = a_room.y + k;

			if (a_tiles[xCoord * height + yCoord] == TileType::Wall)
			{
				a_tiles[xCoord * height + yCoord] = TileType::Floor;
			}
		}
	}

	int x = a_room.x + roomWidth.Random() - 2;
	int y = a_room.y + roomHeight.Random() - 2;
	if (a_addPlayer)
	{
		a_tiles[x * height + y] = TileType::Player;
		x = a_room.x + roomWidth.Random() - 2;
		y = a_room.y + roomHeight.Random() - 2;
	}
	if (a_addKey)
	{
		a_tiles[x * height + y] = TileType::Key;
		x = a_room.x + roomWidth.Random() - 2;
		y = a_room.y + roomHeight.Random() - 2;
	}
	if (a_addGoal)
	{
		a_tiles[x * height + y] = TileType::Goal;
	}
}

inline void MapGenerator::SetTilesForCorridor(const Corridor & a_corridor, std::vector<TileType>& a_tiles, bool a_addDoor)
{
	int xCoord;
	int yCoord;
	for (int j = 0; j < a_corridor.length; j++)
	{
		xCoord = a_corridor.x;
		yCoord = a_corridor.y;

		bool upDown = false;

		switch (a_corridor.direction)
		{
		case Direction::Up:
			yCoord += j;
			break;
		case Direction::Right:
			xCoord += j;
			upDown = true;
			break;
		case Direction::Down:
			yCoord -= j;
			break;
		case Direction::Left:
			xCoord -= j;
			upDown = true;
			break;
		}

		if (a_addDoor && j == 0)
			a_tiles[xCoord * height + yCoord] = upDown ? TileType::DoorUp : TileType::Door;
		else if (a_tiles[xCoord * height + yCoord] == TileType::Wall)
			a_tiles[xCoord * height + yCoord] = TileType::Floor;
	}
}
