#include "GameObject.h"
#include "Renderer2D.h"
#include "Grid.h"

GameObject::GameObject(std::string a_name, Tile* a_tile, const float a_radius)
	: name(a_name), position(a_tile->worldPos), radius(a_radius), currentTile(a_tile),
	open(true), shouldDestroy(false), isActive(true), texture(nullptr), uv({ 0.0f, 0.0f, 1.0f, 1.0f }) {};

void GameObject::SetCurrentTile(Tile * a_newTile)
{
	Grid* grid;
	if (currentTile != nullptr)
	{
		grid = currentTile->GetGrid();
		grid->SetGO(currentTile->pos, nullptr);
	}
	currentTile = a_newTile;
	if (currentTile != nullptr)
	{
		GameObject* oldGO = currentTile->GetGO();

		if (oldGO != nullptr)
			oldGO->SetCurrentTile(nullptr);

		grid = currentTile->GetGrid();
		position = currentTile->worldPos;
		grid->SetGO(currentTile->pos, this);
	}
}
