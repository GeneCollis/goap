#ifndef MapGenerator_H
#define MapGenerator_H
#include "Random.h"
#include <assert.h>

class IntRange
{
public:
	IntRange() : min(0), max(0) {};
	IntRange(const int& a_min, const int& a_max)
		: min(a_min), max(a_max)
	{
		assert(min <= max && "min must not be greater than max");
	};
	~IntRange() = default;

	inline int Random() { return Random::Range(min, max); }

	int min;
	int max;
};

class MapGenerator
{
public:
	MapGenerator(int a_width, int a_height, int a_roomCount, int a_roomWidthMin,
		int a_roomWidthMax, int a_roomHeightMin, int a_roomHeightMax,
		int a_corridorLengthMin, int a_corridorLengthMax) : width(a_width), height(a_height),
		roomCount(a_roomCount), roomWidth({ a_roomWidthMin, a_roomWidthMax }),
		roomHeight({ a_roomHeightMin, a_roomHeightMax }),
		corridorLength({ a_corridorLengthMin,a_corridorLengthMax }) {};
	~MapGenerator() {};

	enum Direction : int
	{
		Up,
		Right,
		Down,
		Left
	};

	struct Room
	{
		int x;
		int y;
		int width;
		int height;
		Direction enteringCorridor;
	};

	struct Corridor
	{
		int x;
		int y;
		int length;
		Direction direction;

		inline int EndY() const
		{
			if (direction == Right || direction == Left)
				return y;
			if (direction == Up)
				return y + length - 1;
			return y - length + 1;
		}

		inline int EndX() const
		{
			if (direction == Up || direction == Down)
				return x;
			if (direction == Right)
				return x + length - 1;
			return x - length + 1;
		}
	};

	enum TileType
	{
		Floor,
		Wall,
		Door,
		DoorUp,
		Player,
		Key,
		Goal
	};

	std::vector<TileType> GenerateMap();


private:
	inline Room SetupRoomCentre();
	inline Room SetupRoom(const Corridor& a_corridoor);
	inline Corridor SetupCorridor(const Room& a_room, const bool& a_first = false);
	
	inline void SetTilesForRoom(const Room& a_room, std::vector<TileType>& a_tiles, bool a_addPlayer = false, bool a_addKey = false, bool a_addGoal = false);
	inline void SetTilesForCorridor(const Corridor& a_corridor, std::vector<TileType>& a_tiles, bool a_addDoor = false);

	int width;
	int height;

	int roomCount;

	IntRange roomWidth;
	IntRange roomHeight;

	IntRange corridorLength;

	int doorCorridor;
	int keyRoom;
};
#endif // !MapGenerator_H
