#pragma once

#include <vector>

class GameObject;

class GameObjectPool
{
public:
	GameObjectPool(const int a_size = 0);
	~GameObjectPool() = default;

	GameObject* Get();

	void Add(GameObject* a_gameObject);

	void DestroyAll();

private:
	std::vector<GameObject*> pool;
};

