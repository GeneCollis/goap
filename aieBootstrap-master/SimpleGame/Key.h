#ifndef Key_H
#define Key_H
#include "GameObject.h"

class Key : public GameObject
{
public:
	Key() : GameObject("Key") {};
	Key(Tile* a_tile, const float a_radius)
		: GameObject("Key", a_tile, a_radius) {};

	virtual void Update(const float deltaTime, aie::Input* input) {};
	virtual void Draw(aie::Renderer2D* renderer2D, float depth = 0.0f, const bool& a_debug = false) const;
	virtual void ImGui() {};

};
#endif // !Key_H
