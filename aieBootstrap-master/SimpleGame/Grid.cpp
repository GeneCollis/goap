#include "Grid.h"
#include "Renderer2D.h"
#include "TextureMap.h"

#include "imgui_glfw3.h"

#include <string>
#include <list>

class CompareFScore
{
public:
	bool operator()(Tile* a_tile1, Tile* a_tile2) const
	{
		return a_tile1->fScore > a_tile2->fScore;
	}
};

Grid::Grid(const unsigned int a_width, const unsigned int a_height, const unsigned int a_size, const glm::vec2& a_worldPos)
{

	width = a_width;
	height = a_height;

	size = a_size;

	worldPos = a_worldPos;

	gridData = new Tile[width * height];

	// Setup the tile data
	int x, y;
	for (int i = 0; i < width * height; i++)
	{
		x = i / height;
		y = i % height;
		gridData[i].grid = this;
		gridData[i].pos = { x, y };
		gridData[i].worldPos = { worldPos.x + x * size + size / 2.0f, worldPos.y + y * size + size / 2.0f };
		gridData[i].uv = TextureMap::GetUV(TextureMap::Wall);
		gridData[i].dirty = true;
		// RIGHT
		if (x + 1 < width)
		{
			gridData[i].right = &gridData[i + height];
		}
		// LEFT
		if (x - 1 >= 0)
		{
			gridData[i].left = &gridData[i - height];
		}
		// UP
		if (y + 1 < height)
		{
			gridData[i].up = &gridData[i + 1];
		}
		// DOWN
		if (y - 1 >= 0)
		{
			gridData[i].down = &gridData[i - 1];
		}

		availableTiles.insert({ PosToIndex(gridData[i].pos) , &gridData[i] });
	}

	floorTexture = TextureMap::Texture();
	floorUV = TextureMap::GetUV(TextureMap::Floor);
}

Grid::~Grid()
{
	delete[] gridData;
}

bool Grid::SetGO(const glm::ivec2 a_pos, GameObject * a_go)
{
	Tile* tile = Get(a_pos);

	tile->go = a_go;

	SetOpen(*tile, a_go == nullptr ? true : a_go->IsOpen());

	if (a_go != nullptr)
	{
		auto tile = availableTiles.find(PosToIndex(a_pos));
		if (tile != availableTiles.end())
		{
			availableTiles.erase(tile);
		}
	}
	else
	{
		availableTiles.insert({ PosToIndex(a_pos) , tile });
	}

	return true;
}

bool Grid::SetGO(const int a_index, GameObject * a_go)
{
	return SetGO(IndexToPos(a_index), a_go);
}

void Grid::SetOpen(Tile & a_tile, const bool a_open)
{
	if (a_tile.open != a_open)
	{
		a_tile.open = a_open;
		a_tile.dirty = true;
		if (a_tile.up != nullptr)
			a_tile.up->dirty = true;
		if (a_tile.right != nullptr)
			a_tile.right->dirty = true;
		if (a_tile.down != nullptr)
			a_tile.down->dirty = true;
		if (a_tile.left != nullptr)
			a_tile.left->dirty = true;
		GridChanged();
	}
}

void Grid::Draw(aie::Renderer2D* a_renderer, const glm::vec2& a_mouseWorldPos, const bool a_debug, const float a_depth /* = (0.0f)*/)
{
	// Draw BG box
	a_renderer->setRenderColour(0.5f, 0.5f, 0.5f); // Dark Grey
	a_renderer->drawBox(worldPos.x + (float)width * size / 2, worldPos.y + (float)height * size / 2, (float)width * size, (float)height * size, 0.0f, a_depth);

	int borderSize = 1;
	float x = size / 2.0f - borderSize + worldPos.x;
	float y = size / 2.0f - borderSize + worldPos.y;
	glm::vec2 mousePos = a_mouseWorldPos - worldPos;
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			const Tile& tile = gridData[i * height + j];

			a_renderer->setRenderColour(1.0f, 1.0f, 1.0f); // White
			if (i == int(mousePos.x / size) && j == int(mousePos.y / size))
			{
				a_renderer->setRenderColour(0.8f, 0.8f, 1.0f); // Light Blue
			}
			else if (tile.IsOpen() == false)
			{
				a_renderer->setRenderColour(1.0f, 0.8f, 0.8f); // Light Red
			}
			else if (a_debug)
			{
				if (tile.traversed)
				{
					a_renderer->setRenderColour(0.7f, 1.0f, 0.7f); // Light Green
				}
				else if (tile.inQueue)
				{
					a_renderer->setRenderColour(0.8f, 0.9f, 1.0f); // Light Blue
				}
			}

			// Draw the box in the position
			//a_renderer->drawBox(x + borderSize, y + borderSize, (float)size - borderSize * 2, (float)size - borderSize * 2, 0.0f, a_depth);
			//if (tile.IsOpen())
			{
				a_renderer->setUVRect(floorUV);
				a_renderer->drawSprite(floorTexture, x + 1, y + 1, size, size, 0.0f, a_depth);
			}
			// Move up the y value
			y += size;
		}
		// Reset the y value
		y = size / 2.0f - borderSize + worldPos.y;
		// Move the x left
		x += size;
	}

}

void Grid::ImGui(Tile * a_mouseTile)
{
	ImGui::Begin("Grid");

	ImGui::Text("Tile Under Mouse:");

	if (a_mouseTile == nullptr)
	{
		ImGui::Text("None");
	}
	else
	{
		std::string s = "XY ";
		s.append(std::to_string(a_mouseTile->pos.x)).append(" , ").append(std::to_string(a_mouseTile->pos.y));
		ImGui::Text(&s[0]);

		ImGui::Text(a_mouseTile->open ? "Open" : "Closed");
		ImGui::Text(a_mouseTile->goal ? "goal: True" : "goal: False");
		ImGui::Text(a_mouseTile->traversed ? "traversed: True" : "traversed: False");
		ImGui::Text(a_mouseTile->inQueue ? "inQueue: True" : "inQueue: False");

		s = "fScore ";
		s.append(std::to_string(a_mouseTile->fScore));
		ImGui::Text(&(s[0]));

		s = "gScore ";
		s.append(std::to_string(a_mouseTile->gScore));
		ImGui::Text(&s[0]);

		s = "hScore ";
		s.append(std::to_string(a_mouseTile->hScore));
		ImGui::Text(&s[0]);

	}
	ImGui::End();
}

std::vector<Tile*> Grid::CalculatePath(Tile* a_startTile, Tile* a_goalTile)
{
	CalculatePathStepStart(a_startTile, std::vector<Tile*>{a_goalTile});

	std::vector<Tile*> path;

	while (CalculatePathStep(path) == false)
	{

	}

	return std::move(path);
}

std::vector<Tile*> Grid::CalculatePath(Tile * a_startTile, const std::vector<Tile*>& a_goalTiles)
{
	CalculatePathStepStart(a_startTile, a_goalTiles);

	std::vector<Tile*> path;

	while (CalculatePathStep(path) == false)
	{

	}

	return std::move(path);
}

void Grid::CalculatePathStepStart(Tile * a_startTile, const std::vector<Tile*>& a_goalTiles)
{
	pf_startTile = a_startTile;
	pf_goalTiles = a_goalTiles;

	pf_currentTile = nullptr;

	// Reset every tiles values
	for (size_t i = 0; i < width * height; ++i)
	{
		pf_currentTile = &gridData[i];

		if (pf_currentTile->open == true)
		{
			pf_currentTile->goal = false;
			pf_currentTile->traversed = false;
			pf_currentTile->inQueue = false;
			pf_currentTile->gScore = std::numeric_limits<float>::max();
			pf_currentTile->prevTile = nullptr;

			pf_currentTile->fScore = std::numeric_limits<float>::max();

			pf_currentTile->hScore = glm::distance(pf_currentTile->worldPos, pf_startTile->worldPos);
			//float hScore;
			//for (size_t i = 1; i < pf_goalTiles.size(); ++i)
			//{
			//	hScore = glm::distance(pf_currentTile->worldPos, pf_goalTiles[i]->worldPos);
			//	if (pf_currentTile->hScore > hScore)
			//		pf_currentTile->hScore = hScore;
			//}
		}
	}

	pf_queue.clear();

	// Add goalTiles the queue
	for (size_t i = 0; i < pf_goalTiles.size(); ++i)
	{
		pf_queue.push_back(pf_goalTiles[i]);
		pf_goalTiles[i]->inQueue = true;
		pf_goalTiles[i]->goal = true;
		pf_goalTiles[i]->prevTile = nullptr;
		pf_goalTiles[i]->gScore = 0;
		pf_goalTiles[i]->fScore = pf_goalTiles[i]->hScore;
	}

	pf_reachedGoal = false;
}

bool Grid::CalculatePathStep(std::vector<Tile*>& a_path)
{
	if (pf_queue.empty() == false)
	{
		CompareFScore comparison;

		// Sort the queue by fScore
		pf_queue.sort(comparison);

		pf_currentTile = pf_queue.back();

		// Check if we reached the goal
		if (pf_currentTile == pf_startTile)
		{
			pf_reachedGoal = true;
		}
		else
		{
			pf_queue.pop_back();
			pf_currentTile->inQueue = false;
			pf_currentTile->traversed = true;

			for (int i = 0; i < 4; ++i)
			{
				Tile* newTile = nullptr;
				switch (i)
				{
				case 0:
					newTile = pf_currentTile->up;
					break;
				case 1:
					newTile = pf_currentTile->right;
					break;
				case 2:
					newTile = pf_currentTile->down;
					break;
				case 3:
					newTile = pf_currentTile->left;
					break;
				default:
					break;
				}

				// If it exists, is open, and hasn't been traversed
				if (newTile != nullptr && newTile->open && newTile->traversed == false && newTile->goal == false)
				{
					// Calculate new gScore based on currentNode's gScore + currentEdge's cost
					float newGScore = pf_currentTile->gScore + newTile->cost;
					// Calculate new fScore based on newGscore + currentEdge's end's hScore
					float newFScore = newGScore + newTile->hScore;

					if (newFScore < newTile->fScore)
					{
						// Setup the values and put it in the queue
						newTile->prevTile = pf_currentTile;
						newTile->gScore = newGScore;
						newTile->fScore = newFScore;

						if (pf_currentTile->inQueue == false)
						{
							pf_queue.push_front(newTile);
							newTile->inQueue = true;
						}
					}
				}
			}
		}
	}
	else
	{
		a_path.clear();
		return true;
	}

	if (pf_reachedGoal)
	{
		a_path.clear();

		pf_currentTile = pf_startTile;

		// Add all of the tiles until we reach the startTile
		while (pf_currentTile != nullptr)
		{
			a_path.push_back(pf_currentTile);
			pf_currentTile = pf_currentTile->prevTile;
		}

		std::reverse(std::begin(a_path), std::end(a_path));
		return true;
	}

	return false;
}

void Grid::GridChanged()
{
	for (auto const& ftn : changedCallback)
	{
		ftn.second();
	}
	for (size_t i = 0; i < width * height; i++)
	{
		gridData[i].dirty = false;
	}
}

void Tile::SetOpen(bool a_open)
{
	grid->SetOpen(*this, a_open);
}
