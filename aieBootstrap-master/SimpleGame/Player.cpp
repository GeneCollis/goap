#include "Player.h"
#include "Key.h"

#include "Input.h"
#include "Renderer2D.h"

#include <imgui.h>

void Player::Update(const float deltaTime, aie::Input * input)
{
	Movement(input, deltaTime);
}

void Player::Draw(aie::Renderer2D * renderer2D, float depth, const bool& a_debug) const
{
	renderer2D->setRenderColour(1.0f, 1.0f, 1.0f, 1.0f);
	renderer2D->drawCircle(position.x, position.y, radius, depth);
}

void Player::ImGui()
{
	ImGui::Begin("Player");

	ImGui::DragFloat2("Position", &position[0]);

	ImGui::Checkbox("Has Key", &hasKey);

	ImGui::End();
}

void Player::Movement(aie::Input * input, const float &deltaTime)
{
	// Setup a direction vector
	glm::vec2 direction = { 0, 0 };

	// Modify the direction depending on the input
	if (input->isKeyDown(aie::INPUT_KEY_W)) // Up
		direction.y++;
	if (input->isKeyDown(aie::INPUT_KEY_A)) // Left
		direction.x--;
	if (input->isKeyDown(aie::INPUT_KEY_S)) // Down
		direction.y--;
	if (input->isKeyDown(aie::INPUT_KEY_D)) // Right
		direction.x++;

	// Don't normalize if there is no length
	if (direction != glm::vec2{ 0, 0 })
		// Make sure the direction is a unit vector
		direction = glm::normalize(direction);

	// Update the position to move in that direction
	position += direction * speed * deltaTime;
}

bool Player::PickupKey(Key * key)
{
	// Check the distance to the key
	// If it is overlapping with the key we can pick it up
	if (glm::distance(position, key->GetPosition()) - radius - key->GetRadius() < 0)
	{
		hasKey = true;
		// destroy the key
		key->Destroy();
		return true;
	}
	return false;
}
