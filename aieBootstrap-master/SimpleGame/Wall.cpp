#include "Wall.h"
#include "Renderer2D.h"
#include "TextureMap.h"

Wall::Wall(glm::vec2 a_pos, float a_size)
	: GameObject("Wall", a_pos, a_size)
{
	open = false;

	texture = TextureMap::Texture();
	uv = TextureMap::GetUV(TextureMap::Wall);
}

void Wall::Draw(aie::Renderer2D * a_renderer2D, float a_depth, const bool& a_debug) const
{
	//a_renderer2D->setRenderColour(0.2f, 0.2f, 0.2f, 1.0f);
	//a_renderer2D->drawBox(position.x, position.y, radius, radius, 0.0f, depth);

	a_renderer2D->setRenderColour(1.0f, 1.0f, 1.0f, 1.0f);
	a_renderer2D->setUVRect(uv);
	a_renderer2D->drawSprite(texture, position.x, position.y, TextureMap::TileSize(), TextureMap::TileSize(), 0.0f, a_depth);
};
